# ReaderBarFP
Este repositorio contiene el codigo fuente para una aplicación desarrollada en C# y MAUI. Esta permite escanear códigos de barras, agregar y administrar facturas, API y usuarios. Utiliza una base de datos SQLite para almacenar los datos y se comunica con una API externa para enviar y recibir información a un servidor relacionada con las facturas.
***

```mermaid
classDiagram
%% https://mermaid.js.org/syntax/classDiagram.html
direction BT
class Factura{
    +Id: int
    +NoFactura: string
    +Notes: string
    +Date: string  	  
    +Registrado: bool	  
    +FacturaValida: bool
    +ResultApi: string  
}
Factura <.. FacturaRepositorio
class Usuarios{
    +Id: int         
    +User: string    
    +Password: string
    +Default: bool   
    +Chofer: string  
    +UserApi: bool   
    +UserAdmin: bool  
}
Usuarios <.. FacturaRepositorio
class Apis{
    +Id: int       
    +Name: string  
    +Api: string   
    +Default: bool   
}
Apis <.. FacturaRepositorio
class FacturaRepositorio{
    +_dbPath: string 		
    +StatusMessage: string 
    -conn: SQLiteConnection

    -Init() 					
    +FacturaRepositorio()		
    -FacturaIsValid(): bool 	
    +AddNewFactura(): bool 	
    +ActualizarRegistro()     
    +GetAllTable< T>(): List~T~
    +AddUpdateUser() 			
    +AddUpdateApi() 			
    +FindUser(): Usuarios 	
    +FindApi(): Apis 			
    +FindFactura(): Factura 	
    +DeleleUser() 			
    +DeleleApi() 				
    -ChangeDefault() 			
    -ChangeDefaultApi()		    
}
FacturaRepositorio <.. ReadBar
FacturaRepositorio <.. Registros

class ReadBar{
    -barcodeResult: Label 				
    -newNotes: Entry						
    -cameraView: CameraPreview			
    -activityIndicator: ActivityIndicator

    +ReadBar()     				
    #OnAppearing() 				
    -CameraView_CamerasLoaded()	
    #CameraView_BarcodeDetected()	
    +AgregarFacturaButton()		   
}
ReadBar ..> ApiRestFP
class ApiRestFP{
    +MESSAGE: string

    +AgregarFacturaApi(): Task<string>
    +ConectivityStatus(): bool		   
}
ApiRestFP ..> FacturaRepositorio

class Registros{
    -Admin: bool    					
    -facturaList: ListView			
    -activityIndicator: ActivityIndicator

    +Registros()				
    #OnAppearing() 			
    -ActualizarRegFac()		
    -CargaMasivaFacturapiAsync()
    -DisplayAction()					   
}
Registros ..> ApiRestFP

class ApiList{
    -apisList: ListView

    +ApiList()     	  
    #OnAppearing() 	  
    +ActualizarListApi()
    -AddApiButton() 	  
    -DisplayAction()	   					   
}
ApiList ..> FacturaRepositorio

class RegApi{
    +ID: int        	
    -nameApi: Entry 	
    -regApi: Entry  	
    -defaultApi: CheckBox

    +RegApi() 	
    -AplicarButton()
    -CerrarButton() 	   					   
}
RegApi ..> FacturaRepositorio

class RegUser{
    -isPasswordVisible: bool 
    -ID: int 				

    +RegUser() 				  
    -AplicarButton() 			  
    -CerrarButton() 			  
    -PasswordVisibility_Tapped() 	   					   
}
RegUser ..> FacturaRepositorio

class UpdateRegistros{
    +ID: int         
    +Factura: Factura 				

    +UpdateRegistros()
    -AplicarButton()  
    -CerrarButton()	   					   
}
UpdateRegistros ..> FacturaRepositorio

class UsersList{
    +UsersList() 		  
    #OnAppearing() 	  
    +ActualizarRegUser()
    -AddUserButton() 	  
    -DisplayAction() 	  	   					   
}
UsersList ..> FacturaRepositorio

class Configuracion{
    -isPasswordVisible: bool 		 
    -user: Entry    				 
    -password: Entry				 
    -PasswordVisibilityImage: Image

    +Configuracion()			
    #OnAppearing()  			
    -RegButton()				
    -AdminButton()			
    -PasswordVisibility_Tapped()					   
}
class AdminPag{
    +AdminPage() 
    -UserButton()
    -ApiButton() 
    -RegButton() 					   
}
```

>### Class Factura, Class Usuarios, Class Apis
Estas clases se utilizan para definir la estructura de las tablas en una base de datos SQLite.


>### Class FacturaRepositorio
La clase proporcionada es una clase de repositorio que interactúa con una base de datos SQLite. Contiene varios métodos para realizar operaciones CRUD (Crear, Leer, Actualizar, Eliminar) en las tablas , y de la base de datos. "Factura" "Usuarios" "Apis"

Aquí hay un desglose de lo que hace cada método:
* **Init()**: este método inicializa la conexión SQLite y crea las tablas necesarias si aún no existen. También inserta algunos datos iniciales en las tablas.
* **FacturaIsValid()**: este método privado comprueba si una cadena determinada es válida. Actualmente, siempre devuelve TRUE, pero hay líneas de código comentadas que sugieren que se podría implementar una lógica de validación adicional.
* **AddNewFactura()**: este método agrega un nuevo registro a la tabla con los valores proporcionados. Primero verifica si el es válido usando el método. Si es válido, inserta el nuevo registro y devuelve . De lo contrario, establece la propiedad para indicar que se requieren datos válidos y devuelve.
* **ActualizarRegistro()**: este método actualiza un registro existente en la tabla con los valores proporcionados. Primero recupera el objeto correspondiente en función del valor. Luego, actualiza las propiedades del objeto y guarda los cambios en la base de datos.
* **GetAllTable<T>()**: este método recupera todos los registros de una tabla específica y los devuelve como una lista de objetos de tipo Table<T>.
* **AddUpdateUser()**: este método agrega un nuevo usuario a la tabla o actualiza un usuario existente. Si el parámetro es 0, se inserta un nuevo usuario con los valores proporcionados. De lo contrario, el usuario existente se actualiza con los nuevos valores.
* **AddUpdateApi()**: este método agrega una nueva API a la tabla o actualiza una API existente. Sigue una lógica similar al método "AddUpdateUser."
* **FindUser()**: este método recupera un usuario de la tabla según los criterios de búsqueda especificados. Si es "Predeterminado", devuelve al usuario con la propiedad establecida en verdadero. De lo contrario, devuelve el usuario con el (id) especificado.
* **FindApi()**: este método recupera una API de la tabla según los criterios de búsqueda especificados. Si es "Predeterminado", devuelve la API con la propiedad establecida en verdadero. De lo contrario, devuelve la API con el (id) especificado.
* **FindFactura()**: Este método recupera una factura de la tabla según el (id) especificado.
* **DeleleUser()**: este método elimina un usuario de la tabla según el especificado.
* **DeleleApi()**: este método elimina una API de la tabla según el especificado.
* **ChangeDefault()**: este método privado se usa internamente para cambiar la propiedad de los usuarios en función del valor proporcionado.
* **ChangeDefaultApi()**: este método privado se usa internamente para cambiar la propiedad de las API en función del valor proporcionado.


>### Class ReadBar
La clase proporciona una interfaz de usuario simple para escanear códigos de barras con la cámara del dispositivo y agregarlos a una base de datos SQLite. También llama a una API externa para agregar nuevos registros al servidor.

Aquí hay un desglose de lo que hace cada método:

* **ReadBar()**: Este es el constructor de la clase. Llama al método para inicializar los componentes de la interfaz de usuario.
* **OnAppearing()**: Este método se llama cuando la página aparece en la pantalla. Restablece los campos de texto y establece el factor de zoom de la cámara en 0,5 e inicia la cámara.
* **CameraView_CamerasLoaded()**: Este método se llama cuando se carga la vista de la cámara. Simplemente llama al método "OnAppearing()".
* **CameraView_BarcodeDetected()**: Este método se llama cuando la cámara detecta un código de barras. Recupera el texto del código de barras detectado y establece el campo de texto para mostrarlo.
* **AgregarFacturaButton()**: Este método se llama cuando el usuario presiona el botón "Agregar Factura". Recupera los valores de los campos de texto y llama al método del objeto para agregar un nuevo registro a la tabla en la base de datos SQLite. Si el método devuelve, muestra una alerta con la propiedad del objeto. De lo contrario, verifica si el dispositivo está conectado a Internet. Si es así, agrega el nuevo registro a travez de una API externa y actualiza el registro en la tabla con el resultado de la llamada a la API. Si la llamada API falla, muestra una alerta con un mensaje apropiado. Si el dispositivo no está conectado a Internet, actualiza el registro para ser validado luego.

>### Class Registros
La clase proporciona una interfaz de usuario para mostrar e interactuar con registros de una base de datos SQLite. Incluye funcionalidad para realizar operaciones CRUD, filtrar y ordenar los registros, realizar operaciones masivas en los registros, navegar a una página separada para actualizar registros individuales e interactuar con una API externa.

Aquí hay un desglose de lo que hace cada método:

* **Registros()**: Este es el constructor de la clase. Llama al método para inicializar los componentes de la interfaz de usuario. También establece el campo en función del valor pasado al constructor para ejecutarse como admin o usuario.
* **OnAppearing()**: Este método se llama cuando la página aparece en la pantalla. Llama al método para actualizar la lista de registros.
* **ActualizarRegFac()**: este método recupera registros de la tabla en la base de datos SQLite y actualiza ListView con los registros recuperados. Si el campo es admin muestra todos los registros. En caso contrario, filtra los registros en función de determinados criterios (vigencia y rango de fechas) y los ordena de forma descendente.
* **CargaMasivaFacturapiAsync()**: Este método se llama cuando el usuario presiona el botón "Sincronizar". Recupera todos los registros de la tabla y los itera. Para cada registro, verifica si cumple con ciertas condiciones (vigencia y estado de registro). Si se cumplen las condiciones, llama al método para agregar el registro a una API externa. Luego actualiza el registro en la tabla según el resultado de la llamada a la API. Después de procesar todos los registros, muestra una alerta con el resumen de la operación.
* **DisplayAction()**: Este método se llama cuando el usuario toca un elemento de cuadrícula en ListView. Recupera el registro seleccionado de la tabla según el parámetro de comando del elemento tocado. Luego navega a una nueva página ("UpdateRegistros()") y pasa el registro seleccionado como parámetro.

>### Class ApiRestFP
La clase contiene métodos para interactuar con una API externa y verificar el estado de conectividad.ApiRestFP

Aquí hay un desglose de lo que hace cada método:

* **AgregarFacturaApi()**: Este método se utiliza para agregar una factura a la API externa. Recupera la URL del punto final de la API del objeto, junto con las credenciales del usuario. Luego construye la URL con los parámetros necesarios y realiza una solicitud HTTP GET a la API usando la clase. La respuesta se deserializa en una lista de objetos y la propiedad del primer objeto se devuelve como una cadena.
* **ConectivityStatus()**: este método comprueba el estado de conectividad del dispositivo. Utiliza la propiedad de la biblioteca para determinar si el dispositivo tiene acceso a Internet.

>### Class ApiList
La clase representa una página de contenido, contiene métodos para actualizar y mostrar una lista de API.

Aquí hay un desglose de lo que hace cada método:

* **ApiList()**: Este es el constructor de la clase. Inicializa el componente y llama al método ActualizarListApi() para completar la lista de API.
* **OnAppearing()**: Este método se llama cuando la página está a punto de aparecer en la pantalla. Llama al método ActualizarListApi() para actualizar la lista de API.
* **ActualizarListApi()**: este método recupera todas las API del objeto y establece la propiedad del control en la lista de API.
* **AddApiButton()**: Este método se llama cuando el usuario presiona el botón "Agregar". Navega a la página para agregar una nueva API.
* **DisplayAction()**: este método se llama cuando el usuario toca una API en la lista. Muestra una hoja de acción con opciones para editar o eliminar la API seleccionada. Si el usuario selecciona "Editar", navega a la página para editar la API. Si el usuario selecciona "Eliminar", elimina la API del objeto y actualiza la lista de API.

>### Class RegApi
 La clase representa una página de contenido, proporciona funcionalidad para agregar o actualizar una API en la base de datos.

 Aquí hay un desglose de lo que hace cada método:

* **RegApi()**: Este es el constructor de la clase. Inicializa el componente y establece los valores de los controles en función de los valores pasados ​​al constructor.
* **AplicarButton()**: Este método se llama cuando el usuario toca el botón "Aplicar". Comprueba si los controles no están vacíos y luego llama al método del objeto para agregar o actualizar la API. Luego muestra un mensaje con el estado de la operación y cierra la página actual de la pila de navegación.
* **CerrarButton()**: Este método se llama cuando el usuario toca el botón "Cerrar". Cierra la página actual de la pila de navegación.

>### Class RegUser
La clase representa una página de contenido. Contiene métodos para agregar y actualizar un usuario.

Aquí hay un desglose de lo que hace cada método:

* **RegUser()**: Este es el constructor de la clase.
* **AplicarButton()**: Este método se llama cuando se hace clic en el botón "Aplicar". Comprueba si los campos de usuario y contraseña no están vacíos y luego llama al método de la clase para agregar o actualizar un usuario. Luego muestra una alerta con el mensaje de estado devuelto por la clase.
* **CerrarButton()**: Este método se llama cuando se hace clic en el botón "Cerrar". Cierra la página actual de la pila de navegación.
* **PasswordVisibility_Tapped()**: Este método se llama cuando se toca el icono de visibilidad de la contraseña. Alterna la visibilidad del campo de contraseña y cambia el icono en consecuencia.

>### Class UpdateRegistros
La clase representa una página de contenido que contiene métodos para mostrar y actualizar un registro.

Aquí hay un desglose de lo que hace cada método:

* **UpdateRegistros()**: Este es el constructor de la clase. Llama al método para inicializar los componentes de la interfaz de usuario. También establece los campos en función del valor pasado al constructor para ejecutarse como admin o usuario.
* **AplicarButton()**:Este método se llama cuando se hace clic en el botón "Aplicar". Actualiza un registro en la base de datos con los valores ingresados ​​en los controles. Luego muestra un mensaje que indica que el registro se ha modificado y cierra la página actual de la pila de navegación.
* **CerrarButton()**:Este método se llama cuando se hace clic en el botón "Cerrar". Cierra la página actual de la pila de navegación.


>### Class UsersList
La clase representa una página de contenido que contiene métodos para mostrar y actualizar una lista de usuarios.

Aquí hay un desglose de lo que hace cada método:

* **UsersList()**: método constructor que inicializa el componente y llama al método para actualizar la lista de usuarios.
* **OnAppearing()**: anula el método de la clase base para actualizar la lista de usuarios cuando aparece la página.
* **ActualizarRegUser()**: método que recupera todos los usuarios de la base de datos y actualiza la lista de usuarios.
* **AddUserButton()**: método de controlador de eventos que navega a la página para agregar un nuevo usuario.
* **DisplayAction()**: método de controlador de eventos que muestra una hoja de acción con opciones para editar o eliminar un usuario. Si el usuario selecciona la opción de edición, el método navega a la página para editar el usuario seleccionado. Si el usuario selecciona la opción de eliminar, el método elimina al usuario seleccionado de la base de datos y actualiza la lista de usuarios.

>### Class Configuracion
La clase representa una página de contenido que contiene métodos para manejar la autenticación de usuarios y alternar la visibilidad de la contraseña.

Aquí hay un desglose de lo que hace cada método:

* **Configuracion()**: Este es el constructor de la clase. Inicializa el componente.
* **OnAppearing()**: Este método se llama cuando la página aparece en la pantalla. Borra los controles.
* **RegButton()**: Este método se llama cuando el usuario toca el botón "Registros". Navega a la página "Registros()".
* **AdminButton()**: Este método se llama cuando el usuario toca el botón "Administrar". Comprueba si el nombre de usuario y la contraseña ingresados ​​coinciden con las credenciales de administrador codificadas. Si coinciden, navega al archivo . Si no, muestra un mensaje de error.
* **PasswordVisibility_Tapped()**: Este método se llama cuando el usuario toca el icono de visibilidad de la contraseña. Alterna la visibilidad de la contraseña cambiando la propiedad del control.


>### Class AdminPage
La clase representa una página de contenido que contiene métodos para navegar a diferentes páginas dentro de la aplicación. Permite al usuario navegar a la lista de usuarios, la lista de API y las páginas de registro.

Aquí hay un desglose de lo que hace cada método:

* **AdminPage()**: Este es el constructor de la clase. Inicializa el componente.
* **UserButton()**: Este método se llama cuando el usuario toca el botón "Usuario". Navega a la página UsersList().
* **ApiButton()**: Este método se llama cuando el usuario toca el botón "Api". Navega a la página ApiList(). 
* **RegButton()**: Este método se llama cuando el usuario toca el botón "Registrar". Navega a la página Registros(true) con el parámetro, lo que indica que es una página de administración.
